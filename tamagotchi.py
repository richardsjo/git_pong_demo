from threading import Timer
import time

class Tamagotchi():

    start_time = time.time()
    elapsed_time = time.time() - start_time 

    def __init__(self):
        self.hungriness = 50
        self.fullness = 50
        self.tiredness = 0
        self.happiness = 0

    def get_hunger(self):
        return self.hungriness

    def get_fullness(self):
        return self.fullness

    def get_tiredness(self):
        return self.tiredness

    def get_happiness(self):
        return self.happiness


    def feeding(self):
        self.hungriness -= 10
        self.fullness += 10
         
    def play(self):
        self.tiredness += 10
        self.happiness += 10
         
    def sleep(self):
        self.tiredness -= 10

    def poop(self):
        self.fullness -= 10

    def timePassed(self):
        while True:
            self.tiredness += 10
            self.happiness -= 10
            self.hungriness += 10 
        
            time.sleep(2)
            
            if self.hungriness > 70 and self.hungriness <= 100:
                print("hangry")
                
            elif self.hungriness > 100:
                print("dead")
                
    
        
if __name__ == '__main__':
    t1 = Tamagotchi()
    t = Timer(2.0, t1.timePassed)
    t.start()
import unittest
from tamagotchi import hunger, fullness

class testTamagotchi(unittest.TestCase):
    def test_default_hunger_50(self):
        self.assertEqual(hunger(), 50)

    def test_default_fullness_50(self):
        self.assertEqual(fullness(), 50)


if __name__ == "__main__":
    unittest.main()